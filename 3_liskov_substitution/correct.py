from typing import List
from abc import ABC, abstractmethod


'''
Para que este método cumpla con el principio, seguiremos estos principios:

- Si la superclase (Car) tiene un método que acepta un parámetro del tipo de la superclase (Car), 
entonces su subclase (Renault) debería aceptar como argumento un tipo de la superclase (Car) 
o un tipo de la subclase (Renault).
- Si la superclase devuelve un tipo de ella misma (Car), entonces su subclase (Renault) 
debería devolver un tipo de la superclase (Car) o un tipo de la subclase (Renault).
'''


class Car(ABC):

    @abstractmethod
    def get_amount_seats(self) -> int:
        pass


class Renult(Car):

    def get_amount_seats(self) -> int:
        return 4


class Toyota(Car):

    def get_amount_seats(self) -> int:
        return 7


class Chevrolet(Car):

    def get_amount_seats(self) -> int:
        return 4


def print_amount_seats(car_list: List[Car]) -> None:
    for car in car_list:
        print(car.get_amount_seats())


if __name__ == '__main__':
    car_list = [Renult(), Toyota(), Chevrolet()]
    print_amount_seats(car_list)


'''
Ahora al método print_amount_seats no le importa el tipo de la clase, simplemente llama al método 
get_amount_seats() de la superclase. Solo sabe que el parámetro es de tipo Car, ya sea Car 
o alguna de las subclases.

Como podemos ver, ahora el método print_amount_seats() no necesita saber con qué tipo de Car va 
a realizar su lógica, simplemente llama al método get_amount_seats() del tipo Car, 
ya que por contrato cada subclase de Car debe implementar dicho método.
'''

from typing import List
from abc import ABC, abstractmethod


class Car(ABC):

    pass


class Renult(Car):

    def get_amount_seats_renult(self) -> int:
        return 4


class Toyota(Car):

    def get_amount_seats_toyota(self) -> int:
        return 7


def print_amount_seats(car_list: List[Car]) -> None:
    for car in car_list:
        if isinstance(car, Renult):
            print(car.get_amount_seats_renult())
        if isinstance(car, Toyota):
            print(car.get_amount_seats_toyota())


if __name__ == '__main__':
    car_list = [Renult(), Toyota(),]
    print_amount_seats(car_list)


'''
Esto viola tanto el principio de substitución de Liskov como el de abierto/cerrado. 
El programa debe conocer cada tipo de Car y llamar a su método get_amount_seats_renult_x asociado.

Así, si añadimos un nuevo coche, el método debe modificarse para aceptarlo.

    car_list = [Renult(), Toyota(), Chevrolet(),]

Entonces tendremos que agregar el nuevo if:

    if isinstance(car, Chevrolet):
            print(car.get_amount_seats_chevrolet())
'''
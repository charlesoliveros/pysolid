# L: Liskov Substitution Principle - Principio de substitución de Liskov

Declara que una subclase debe ser sustituible por su superclase, y si al hacer esto, 
el programa falla, estaremos violando este principio.

Cumpliendo con este principio se confirmará que nuestro programa tiene una jerarquía 
de clases fácil de entender y un código reutilizable.
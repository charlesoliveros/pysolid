# O: Open/Closed Principle - Principio abierto/cerrado

Establece que las entidades software (clases, módulos y funciones) deberían estar abiertos para su extensión, 
pero cerrados para su modificación.

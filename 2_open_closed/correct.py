from typing import List
from abc import ABC, abstractmethod


'''
Para que cumpla con este principio podríamos hacer lo siguiente:
Cada carro extiende la clase abstracta Car e implementa el método abstracto get_value().

Así, cada coche tiene su propia implementación del método get_value(), 
por lo que el método print_car_values() itera el listado de carros y  
solo llama al método get_value().

Ahora, si añadimos un nuevo coche, get_value() no tendrá que ser modificado. 
Solo tendremos que añadir el nuevo carro al listado, cumpliendo así el principio abierto/cerrado.
'''


class Car(ABC):

    @abstractmethod
    def get_value(self) -> int:
        pass


class Renult(Car):

    def get_value(self) -> int:
        return 4000


class Toyota(Car):

    def get_value(self) -> int:
        return 5000


class Chevrolet(Car):

    def get_value(self) -> int:
        return 4500


def print_car_values(car_list: List[Car]) -> None:
    for car in car_list:
        print(car.get_value())


if __name__ == '__main__':
    car_list = [Renult(), Toyota(), Chevrolet()]
    print_car_values(car_list)

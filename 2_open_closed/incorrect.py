from typing import List


class Car: 
 
    brand: str

    def __init__(self, brand: str):
        self.brand = brand

    def get_brand(self) -> str:
        return self.brand

    def save_db(self) -> None:
        print(f'Saving the car {self.get_brand()}')
        pass


def print_car_values(car_list: List[Car]) -> None:
    for car in car_list:
        if car.get_brand() == 'Renult':
            print(4000)
        if car.get_brand() == 'Toyota':
            print(5000)


if __name__ == '__main__':
    car_list = [Car('Renult'), Car('Toyota'),]
    print_car_values(car_list)


'''
Esto no cumpliría el principio abierto/cerrado, ya que si decidimos añadir un nuevo coche 
de otra marca:

    car_list = [Car('Renult'), Car('Toyota'), Car('Chevrolet'),]

También tendríamos que modificar el método que hemos creado anteriormente, agregando un
nuevo if:

    if car.get_brand() == 'Chevrolet':
            print(f'{car.get_brand()}: {4500}')
'''
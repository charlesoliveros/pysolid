'''
Supongamos que tenemos una clase para realizar el acceso a datos, y lo hacemos a través de una BBDD:
'''

class DataBaseService:

    def get_data(self) -> list:
        data = []
        print('Getting data from DB')
        return data


class DataAcess:

    data_service: DataBaseService

    def __init__(self, data_service: DataBaseService) -> None:
        self.data_service = data_service

    def get_data(self) -> list:
        return self.data_service.get_data()


if __name__ == '__main__':
    data_access = DataAcess(DataBaseService())
    data = data_access.get_data()


'''
Imaginemos que en el futuro queremos cambiar el servicio de BBDD por un servicio que conecta 
con una API. ¿Ves el problema? Tendríamos que ir modificando todas las instancias de la 
clase DataAcess, una por una.
Esto es debido a que nuestro módulo de alto nivel (DataAcess) depende de un módulo 
de más bajo nivel (DatabaseService), violando así el principio de inversión de dependencias. 
El módulo de alto nivel debería depender de abstracciones.
'''

from abc import ABC, abstractmethod


'''
Para arreglar esto, podemos hacer que el módulo AccesoADatos 
dependa de una abstracción más genérica.
'''


class Connection(ABC):

    @abstractmethod
    def get_data() -> list:
        pass


'''
Ahora, cada servicio que queramos pasar a DataAcess deberá implementar 
la interfaz Conexion:
'''


class DataBaseService(Connection):

    def get_data(self) -> list:
        data = []
        print('Getting data from DB')
        return data


class APIService(Connection):

    def get_data(self) -> list:
        data = []
        print('Getting data from API')
        return data


class DataAcess:

    data_service: Connection

    def __init__(self, data_service: Connection) -> None:
        '''Así, sin importar el tipo de conexión que se le pase al módulo DataAcess, 
        ni este ni sus instancias tendrán que cambiar, por lo que nos ahorraremos mucho trabajo.
        '''
        self.data_service = data_service

    def get_data(self) -> list:
        return self.data_service.get_data()


if __name__ == '__main__':
    '''Esta instancia no necesitó cambiar'''
    data_access = DataAcess(DataBaseService())
    data = data_access.get_data()

    '''Solo debemos pasar el tipo de conexion especifico o subclase'''
    data_access = DataAcess(APIService())
    data = data_access.get_data()


'''
Así, tanto el módulo de alto nivel (DataAcess) como el de bajo nivel (DataBaseService y 
APiService) dependen de abstracciones, por lo que cumplimos el principio de inversión 
de dependencias. Además, esto nos forzará a cumplir el principio de Liskov, 
ya que los tipos derivados de Conexion (DatabaseService y APIService) 
son sustituibles por su abstracción (interfaz Connection).
'''

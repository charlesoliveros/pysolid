class Car:  
    brand: str

    def __init__(self, brand: str):
        self.brand = brand

    def get_brand(self) -> str:
        return self.brand

    def save_db(self) -> None:
        print(f'Saving the car {self.get_brand()}')
        pass


if __name__ == '__main__':
    car = Car('Renult')
    car.save_db()


'''
¿Por qué este código viola el principio de responsabilidad única?

Como podemos observar, la clase Coche permite tanto el acceso a las propiedades de la clase como a realizar 
operaciones sobre la BBDD, por lo que la clase ya tiene más de una responsabilidad.

Supongamos que debemos realizar cambios en los métodos que realizan las operaciones a la BBDD. En este caso, además 
de estos cambios, probablemente tendríamos que tocar los nombres o tipos de las propiedades, métodos, etc, cosa que 
no parece muy eficiente porque solo estamos modificando cosas que tienen que ver con la BBDD, ¿verdad?
'''
'''
Para evitar el mal diseño, debemos separar las responsabilidades de la clase, 
por lo que podemos crear otra clase que se encargue de las operaciones a la BBDD.
Nuestro programa será mucho más cohesivo y estará más encapsulado aplicando este principio.
'''

class Car:  
    brand: str

    def __init__(self, brand: str):
        self.brand = brand

    def get_brand(self) -> str:
        return self.brand


class carDB:

    def save(self, car: Car) -> None:
        print(f'Saving the car {car.get_brand()}')

    def delete(self, car: Car) -> None:
        print(f'Deleting the car {car.get_brand()}')


if __name__ == '__main__':
    car_db = carDB()
    car = Car('Renult')
    car_db.save(car)

from abc import ABC, abstractmethod
'''
Definimos las clases necesarias para albergar algunos tipos de aves. 
Por ejemplo, tendríamos loros, tucanes y halcones:
'''


class Bird(ABC):

    @abstractmethod
    def fly(self) -> None:
        pass

    @abstractmethod
    def eat(self) -> None:
        pass

    @abstractmethod
    def swim(self) -> None:
        pass


class Parrot(Bird):

    def fly(self) -> None:
        print('Parrot is flying')

    def eat(self) -> None:
        print('Parrot is eating')

    def swim(self) -> None:
        '''Problema: el loro no nada'''
        print('Parrot is swimming')


class Toucan(Bird):

    def fly(self) -> None:
        print('Toucan is flying')

    def eat(self) -> None:
        print('Toucan is eating')
    
    def swim(self) -> None:
        print('Toucan is swimming')


class Penguin(Bird):

    def fly(self) -> None:
        '''Problema: El pinguino no vuela'''
        print('Penguin is flying')

    def eat(self) -> None:
        print('Penguin is eating')
    
    def swim(self) -> None:
        print('Penguin is swimming')


if __name__ == '__main__':
    parrot = Parrot()
    parrot.fly()
    parrot.eat()
    parrot.swim()

    toucan = Toucan()
    toucan.fly()
    toucan.eat()
    toucan.swim()

    penguin = Penguin()
    penguin.fly()
    penguin.eat()
    penguin.swim()

'''
El problema es que el loro no nada, y el pingüino no vuela, por lo que tendríamos que añadir 
una excepción o aviso si se intenta llamar a estos métodos. Además, si quisiéramos añadir 
otro método a la interfaz IAve, tendríamos que recorrer cada una de las clases que la implementa 
e ir añadiendo la implementación de dicho método en todas ellas. 
Esto viola el principio de segregación de interfaz, ya que estas clases (los clientes) no 
tienen por qué depender de métodos que no usan.
'''

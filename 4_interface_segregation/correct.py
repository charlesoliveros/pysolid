from abc import ABC, abstractmethod

'''
Lo más correcto sería segregar más las interfaces, tanto como sea necesario.
Así, cada clase implementa las interfaces de la que realmente necesita implementar sus métodos. 
A la hora de añadir nuevas funcionalidades, esto nos ahorrará bastante tiempo, y además, 
cumplimos con el primer principio (Responsabilidad Única).
'''


class Bird(ABC):

    @abstractmethod
    def eat(self) -> None:
        pass


class FlyingBird(ABC):

    @abstractmethod
    def fly(self) -> None:
        pass


class SwimmingBird(ABC):

    @abstractmethod
    def swim(self) -> None:
        pass


class Parrot(Bird, FlyingBird):

    def fly(self) -> None:
        print('Parrot is flying')

    def eat(self) -> None:
        print('Parrot is eating')


class Toucan(Bird, FlyingBird, SwimmingBird):

    def fly(self) -> None:
        print('Toucan is flying')

    def eat(self) -> None:
        print('Toucan is eating')
    
    def swim(self) -> None:
        print('Toucan is swimming')


class Penguin(Bird, SwimmingBird):

    def eat(self) -> None:
        print('Penguin is eating')
    
    def swim(self) -> None:
        print('Penguin is swimming')


if __name__ == '__main__':
    parrot = Parrot()
    parrot.fly()
    parrot.eat()

    toucan = Toucan()
    toucan.fly()
    toucan.eat()
    toucan.swim()

    penguin = Penguin()
    penguin.eat()
    penguin.swim()
